class Field {
  constructor (obj) {
    this.type = "Input";
    this.title = "此字段";
    this.value = "";
    this.required = true;
    this.rules = [];
    Object.assign(this,obj)
  }
  has (keyString) {
    let objectProto = Object.prototype;
    let hasOwnProperty = objectProto.hasOwnProperty;
    return hasOwnProperty.call(this,keyString)
  }  
}

class Register {
  constructor (obj) {
    if(obj) {
      for (let key of Object.keys(obj)) {
        obj[key] = new Field(obj[key])
      }
    }
    Object.assign(this,obj)
  }
  has (keyString) {
    let objectProto = Object.prototype;
    let hasOwnProperty = objectProto.hasOwnProperty;
    return hasOwnProperty.call(this,keyString)
  }
  get (KeyString) {
    return this.has(KeyString)? this[KeyString]: "";
  }
  set(keyString, value) {
    Vue.set(this, keyString, value)
  }
  setFields(formEle) {
    let form = formEle;
    let elements = form && form.elements ? form.elements : [];
    let eleFields = Array.from(elements).filter(element => {
      return element.name && this.has(element.name)
    }).map(element => {
       let field = this.get(element.name);
       element.cn_name = field.title;
      //  element.required = field.required;
       element.isRequired = field.required;
       element.rules = field.rules;
       return element;
    })
    Object.defineProperty(this,"eleFields",{
      value: eleFields,
      enumerable: false
    })
  }
  getFields() {
    return this.eleFields;
  }
}

export default Register
