class validation {
  getResult(value, regExp) {
    if (regExp instanceof RegExp) {
      return value.match(regExp)
    }
  }
  catchRegExp(name) {
    if (this.regExp.hasOwnProperty(name)) {
      return this.regExp[name]
    }
  }
  factory(name) {
    let regExp = this.catchRegExp(name);
    return function (value) {
      if (regExp instanceof RegExp) {
        return value.match(regExp)
      }
    }
  }
  get isValid() {
    return {
      regExp: this.getResult,
      number: this.factory('number'),
      password: this.factory('password'),
      defaultPassword: this.factory('defaultPassword')
    }
  }
  get message() {
    return {
      required: "请填写{name}",
      number: "{name}是数字",
      password: "密码需要由字母、数字或下划线任意组成",
      defaultPassword: "密码长度请设置在6-12位"
    }
  }
  get regExp() {
    return {
      bracket: /\[(.*)\]/i,
      decimal: /^\d*(\.)\d+/,
      email: /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i,
      escape: /[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g,
      flags: /^\/(.*)\/(.*)?/,
      integer: /^\-?\d+$/,
      number: /^\-?\d*(\.\d+)?$/,
      url: /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/i,
      password: /^[a-zA-Z0-9_]{1,}$/,
      defaultPassword: /^\s*[\s\S]{6,12}\s*$/
    }
  }
  prompt(type, name) {
    let prompt = this.message[type]
    if (!prompt) {
      return false;
    }
    if (name) {
      prompt = prompt.replace(/({([^>]+)})/ig, name)
    }
    return prompt;
  }
}

const validHelper = new validation();

class Errors {
  constructor(obj) {
    Object.assign(this, obj)
  }
  has(keyString) {
    let objectProto = Object.prototype;
    let hasOwnProperty = objectProto.hasOwnProperty;
    return hasOwnProperty.call(this, keyString)
  }
  get(keyString) {
    if (keyString) {
      return this.has(keyString) ? this[keyString] : "";
    } else {
      return this
    }
  }
  clear(keyString) {
    if (keyString) {
      Vue.delete(this, keyString)
    } else {
      //clearAll
      for (let key of Object.keys(this)) { 
        Vue.delete(this, key)
      }    
    }
  }
  set(keyString, value) {
    Vue.set(this, keyString, value)
  } 
  valid(ele) {
    let name = ele.name;
    let value = ele.value;
    if (!value && ele.isRequired) {
      this.set(name, validHelper.prompt("required", ele.cn_name))
      return;
    } else {
      this.clear(name);
    }

    let rules = ele.rules;
    if (!rules || rules.length < 1) {
      return;
    }
    rules.every(item => {
      let type = item.type;
      if (validHelper.isValid.hasOwnProperty(type)) {
        let isValidFn = validHelper.isValid[type];
        if (!isValidFn(value)) {
          if (item.message) {
            this.set(name, item.message)
          } else {
            this.set(name, validHelper.prompt(type, ele.cn_name))            
          }
          return false;
        } else {
          if(this.has(name)) {
            this.clear(name);            
          }
          return true;
        }
      }
    })
  }
}

export default Errors