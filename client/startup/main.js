import App from './App.vue'
import router from 'router'
import store from 'store'
import './startup.js'

new Vue({
	router,
  store,
	render: h => h(App)
}).$mount('#app')
