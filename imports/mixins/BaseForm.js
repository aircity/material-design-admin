import Errors from 'model/errors'

export default {
  data: () => ({
    errors: new Errors()
  }),
  methods: {
    errorsHandle(errors) {
      this.errors = errors;
    }
  }
}