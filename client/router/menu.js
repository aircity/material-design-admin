import Mounter from './mounter.vue';

const lazy = (name) => () => System.import(`pages/${name}.vue`);

export default [
  {
    name: 'Account',
    path: '/account',
    component: Mounter,
    meta: {
      title: "Account"
    },
    children: [
      {
        path: '/account/create',
        component: lazy('form'),
        meta: {
          title: 'Create'
        }
      },
      {
        path: '/account/table',
        component: lazy('table'),
        meta: {
          title: 'Table'
        }
      }
    ]
  },
  {
    name: 'Widget',
    path: '/widget',
    component: Mounter,
    meta: {
      title: 'Widget'
    },
    children: [
      {
        path: '/widget/dates',
        component: lazy('dates'),
        meta: {
          title: 'Date Time Picker'
        }
      },
      {
        path: '/widget/modal',
        component: lazy('modal'),
        meta: {
          title: 'Modals'
        }
      },
      {
        path: '/widget/tree',
        component: lazy('tree'),
        meta: {
          title: 'Tree'
        }        
      },
      {
        path: '/widget/try',
        component: lazy('try'),
        meta: {
          title: 'Create'
        }
      }  
    ]
  },
  {
    name: 'UI Element',
    path: '/ui',
    component: Mounter,
    meta: {
      title: 'UI Element'
    },
    //Button Card Checkbox Textfield Toast
    children: [
      {
        path: '/widget/button',
        component: lazy('button'),
        meta: {
          title: 'Button Elements'
        }
      }
    ]
  }
]
