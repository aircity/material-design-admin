export default {
  data() {
    return {
      pageIndex: 0,
      itemCount: 0
    };
  },
  props: {
    pageSize: {type: Number, default: 10},
    value: {type: Array, required: true},
    request: {type: Function, required: true},
    transformer: {
      type: Function,
      default: (pageSize, pageIndex) => ({
        currentPage: pageIndex + 1,
        perPage: pageSize
      })
    }
  },
  computed: {
    items() {
      return this.value;
    },
    beginIndex() {
      return this.itemCount ? this.pageSize * this.pageIndex + 1 : 0;
    },
    endIndex() {
      return Math.min(this.pageSize * (this.pageIndex + 1), this.itemCount);
    },
    pageCount() {
      return Math.floor((this.itemCount - 1) / this.pageSize + 1);
    }
  },
  methods: {
    async query(pageIndex) {
      if (pageIndex === undefined) {
        pageIndex = this.pageIndex;
      }
      try {
        let data = await this.request(
          this.transformer(this.pageSize, pageIndex)
        );
        if (Array.isArray(data)) {
          data = {
            list: data.slice(
              pageIndex * this.pageSize,
              (pageIndex + 1) * this.pageSize
            ),
            count: data.length
          };
        }
        if (data.list.length === 0 && pageIndex > 0) {
          pageIndex = Math.max(0, Math.floor((data.count - 1) / this.pageSize));
          data = await this.query(pageIndex);
        }
        this.items = data.list;
        this.$emit("input", data.list);
        this.itemCount = data.count;
        this.pageIndex = pageIndex;
        return data;
      } catch (e) {
        if (e instanceof RequestError) {
          this.$emit("error", e.message);
        }
        throw e;
      }
    }
  }
};
