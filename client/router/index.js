import Router from 'vue-router'

import menu from './menu';

import Layout from 'layout/layout.vue'
import Login from 'layout/login.vue'

import Waves from 'vue-directive-waves';
Vue.use(Waves);

Vue.use(Router);

const router = new Router({
    mode: 'history',
    base: '/',  
    linkActiveClass: 'active',
    scrollBehavior: () => ({y: 0}),
    routes: [
        {
            path: '/',
            name: 'layout',
            component: Layout,
            children: menu
        }, {
            path: '/login',
            name: 'login',
            component: Login
        }, {
            path: '*',
            redirect: '/'
        }
    ]
})

router.beforeEach((route, redirect, next) => {
    next()
})

router.afterEach((route, redirect) => {})

export default router
