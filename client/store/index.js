import Vuex from 'vuex'
import menu from 'router/menu'

import * as getters from './getters'
import * as actions from './actions'
import * as mutations from './mutations'

Vue.use(Vuex)

const getMenu = function (menu) {
  return menu.map(item => {
    if (item.hasOwnProperty("component")) {
      delete item.component;
    }
    if (item.hasOwnProperty("children")) {
      item.expanded = false;
      item.children = getMenu(item.children);
    }
    return item;
  })
}

const state = {
  menu: getMenu(menu)
}



const store = new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})

if (module.hot) {
  module.hot.accept([
    './getters',
    './actions',
    './mutations'
  ], () => {
    store.hotUpdate({
      getters: require('./getters'),
      actions: require('./actions'),
      mutations: require('./mutations')
    })
  })
}

export default store
