import Input from './input.vue';
Vue.component("Input",Input);
import Select from './select.vue';
Vue.component("Select",Select);
import Dates from './dates.vue';
Vue.component("Dates",Dates);

import Button from './button.vue';
Vue.component("Button",Button);